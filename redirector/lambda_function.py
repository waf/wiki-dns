#!/usr/bin/env python3

import os
import logging

import pg8000.native


def get_env(var):
    return os.environ.get(var, None)


def postgres_connect(user, password, host, db):
    return pg8000.native.Connection(
        user,
        host=host,
        database=db,
        password=password,
        ssl_context=True,
    )


def generate_html(title="title", body="body"):
    return f"""<html>
<head>
<title>{title}</title>
</head>
<body>
{body}
</body>
</html>
"""


def lambda_handler(event, context):
    logger = logging.getLogger()

    level = logging.INFO
    if os.environ.get("LOG_LEVEL"):
        level = logging.DEBUG
    logger.setLevel(level)

    logger.debug(f"lambda event is {event}")

    context = event["requestContext"]
    subdomain = context["domainPrefix"]
    logger.debug(f"subdomain is {subdomain}")

    table = get_env("TABLE")

    connection = postgres_connect(
        get_env("USERNAME"),
        get_env("PASSWORD"),
        get_env("HOST"),
        get_env("DATABASE"),
    )

    if "http" in context:
        method = context["http"]["method"]
    else:
        method = context["httpMethod"]
    logger.debug(f"method is {method}")

    headers = {
        "content-type": "text/html; charset=UTF-8",
    }
    if method in ("GET", "HEAD"):
        query = f"SELECT * FROM {table} WHERE hostname = '{subdomain}'"
        logger.debug(f"query to run: {query}")
        results = connection.run(query)
        logger.debug(f"db results: {results}")

        if results:
            _, url, _ = results[0]

            status_code = 301
            body = generate_html(
                "redirect", f'redirecting to <a href="{url}">{url}</a>'
            )
            headers["Location"] = url
        else:
            status_code = 404
            body = generate_html(title="not found", body=f"subdomain {subdomain} not found")
    elif method == "PUT":
        qs = event["queryStringParameters"]
        hostname = qs["hostname"]
        url = qs["url"]
        query = f"INSERT INTO {table} (hostname, url) VALUES ('{hostname}', '{url}')"
        logger.debug(f"query to run: {query}")
        results = connection.run(query)
        logger.debug(f"db results: {results}")

        status_code = 201
        body = generate_html("posting", "successful post")

    logger.debug(f"status code is {status_code}")
    logger.debug(f"headers to send: {headers}")
    logger.debug(f"body is '''{body}'''")

    return {
        "statusCode": status_code,
        "body": body,
        "headers": headers,
    }
