#!/usr/bin/env python3

import logging
import os


def get_env(var):
    return os.environ.get(var)


def lambda_handler(event, context):
    logger = logging.getLogger()

    level = logging.INFO
    if get_env("LOG_LEVEL"):
        level = logging.DEBUG
    logger.setLevel(level)

    logger.debug(f"event: {event}")

    authorized = False
    effect = "Deny"
    header_name = "headers"
    if header_name in event:
        headers = event[header_name]
        logger.debug(f"headers: {headers}")

        auth_header_names = ["authorization", "Authorization"]
        for auth_header_name in auth_header_names:
            if auth_header_name in headers:
                api_key = headers[auth_header_name]
    elif "type" in event:
        api_key = event["authorizationToken"]
    else:
        raise Exception("event type not supported")
    logger.debug(f"api key is {api_key}")
    if api_key == get_env("API_KEY"):
        authorized = True
        effect = "Allow"

    logger.debug(f"authorized is {authorized}")
    logger.debug(f"effect is {effect}")

    final_format = {
        "context": {
            "authorized": authorized,
        },
    }
    if get_env("FORMAT") == "v2":
        final_format.update(
            {
                "isAuthorized": authorized,
            }
        )
    else:
        final_format.update(
            {
                "principalId": "yyyyyyyy",
                "policyDocument": {
                    "Version": "2012-10-17",
                    "Statement": [
                        {
                            "Action": "execute-api:Invoke",
                            "Effect": effect,
                            "Resource": event["methodArn"],
                        }
                    ],
                },
            }
        )
    logger.debug(f"final format: {final_format}")

    return final_format
